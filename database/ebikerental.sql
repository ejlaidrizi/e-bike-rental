-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 24, 2022 at 08:50 PM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 8.0.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ebikerental`
--

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE `account` (
  `id` int(11) NOT NULL,
  `fullname` text NOT NULL,
  `email` text NOT NULL,
  `phone_number` varchar(255) NOT NULL COMMENT 'Prefix and number (+<number>)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `account`
--

INSERT INTO `account` (`id`, `fullname`, `email`, `phone_number`) VALUES
(1, 'Max Miller', 'max.miller@mail.com', '+1-914-555-0187'),
(2, 'John Smith', 'j.smith@mail.com', '+1-978-555-0138'),
(3, 'Monique Williams', 'monique.williams@mail.com', '+1-857-555-0120'),
(4, 'Sarah Murillo', 'sarah.m@mail.com', '+1-508-555-0197\r\n'),
(5, 'Brian Peterson', 'b.peterson@outlook.com', '+1-617-555-0117\r\n'),
(8, 'Martha Carter', 'm.carter@mail.com', '+1-617-555-0116'),
(10, 'Test User', 'Testuser@gmail.com', '+389 70 123 456');

-- --------------------------------------------------------

--
-- Table structure for table `bank_card`
--

CREATE TABLE `bank_card` (
  `id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `default_type` text DEFAULT NULL COMMENT '<Default> if card is selected as default,else NULL',
  `card_number` varchar(19) NOT NULL,
  `cardholder_name` varchar(255) NOT NULL,
  `cvv` int(3) NOT NULL,
  `balance` float NOT NULL,
  `expiration_date` varchar(5) NOT NULL,
  `account_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `bank_card`
--

INSERT INTO `bank_card` (`id`, `type`, `default_type`, `card_number`, `cardholder_name`, `cvv`, `balance`, `expiration_date`, `account_id`) VALUES
(1, 'MASTERCARD', 'Default', '5299 6400 0000 2300', 'M Miller', 531, 450000, '6/23', 2),
(2, 'VISA\r\n', 'Default', '4111 2156 9000 4806', 'J Smith', 370, 340500, '3/24', 3),
(3, 'MASTERCARD', NULL, '8555 6311 2001 1400', 'M Williams', 211, 85700, '8/23', 1),
(6, 'VISA', NULL, '4228 0210 8390 2227', 'S Murillo', 684, 2407060, '12/24', 4),
(7, 'MAESTRO', 'Default', '8557 2001 9073 6630', 'B Peterson', 597, 560384, '12/24', 5),
(9, 'VISA', 'Default', '5200 4533 9111 1664', 'M Williams', 138, 893058, '9/24', 3);

-- --------------------------------------------------------

--
-- Table structure for table `e_vehicle`
--

CREATE TABLE `e_vehicle` (
  `id` int(11) NOT NULL,
  `model_name` varchar(255) NOT NULL,
  `brand_name` varchar(255) NOT NULL,
  `e_type` text NOT NULL,
  `max_speed` float NOT NULL COMMENT 'Kilometers per Hour (km/h)',
  `max_battery_capacity` int(11) NOT NULL COMMENT 'MiliAmper per Hour (mAh)',
  `current_battery_capacity` int(11) NOT NULL COMMENT 'MiliAmper per Hour (mAh)',
  `range_per_charge` float NOT NULL COMMENT 'Kilometers',
  `minute_price` float NOT NULL COMMENT 'Dollars per minutes ($/m)',
  `status` varchar(255) NOT NULL DEFAULT 'Free' COMMENT 'Free/Rented',
  `location` varchar(555) NOT NULL DEFAULT 'Depo',
  `QR_code` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `e_vehicle`
--

INSERT INTO `e_vehicle` (`id`, `model_name`, `brand_name`, `e_type`, `max_speed`, `max_battery_capacity`, `current_battery_capacity`, `range_per_charge`, `minute_price`, `status`, `location`, `QR_code`) VALUES
(1, 'M365', 'Xiaomi', 'Electric Scooter', 25, 7800, 7700, 30, 0.15, 'Free', 'Checkpoint 1', '100266'),
(2, 'M365 Pro', 'Xiaomi', 'Electric Scooter', 27, 12800, 12700, 40, 0.2, 'Free', 'Checkpoint 1', '421456'),
(3, 'Air Go', 'Pure', 'Electric Scooter', 26, 5200, 5100, 20, 0.17, 'Rented', 'Checkpoint 2', '465435'),
(4, 'Air Pro', 'Pure', 'Electric Scooter', 26, 9800, 9700, 50, 0.19, 'Free', 'Checkpoint 2', '234255'),
(5, 'Air T15', 'Ninebot Segway', 'Electric Scooter', 20, 4000, 3900, 13, 0.13, 'Rented', 'Checkpoint 3', '548736'),
(6, 'Domane+ ALR', 'Trek', 'Electric Bike', 25, 7000, 6900, 104, 0.3, 'Free', 'Checkpoint 1', '786344'),
(7, 'STEREO HYBRID 160 HPC SLT 750 27.5', 'Cube', 'Electric Bike', 20, 17000, 16900, 140, 0.5, 'Rented', 'Checkpoint 3', '738901'),
(8, 'GO PRO', 'Xiaomi', 'Electric Scooter', 27, 5689, 5435, 32, 0.17, 'Free', 'Checkpoint 1', '420253');

-- --------------------------------------------------------

--
-- Table structure for table `ride`
--

CREATE TABLE `ride` (
  `id` int(11) NOT NULL,
  `route_distance` float NOT NULL DEFAULT 0 COMMENT 'Kilometers',
  `start_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `end_time` timestamp NULL DEFAULT NULL,
  `time_rented` varchar(250) DEFAULT NULL,
  `calories` float NOT NULL DEFAULT 0 COMMENT 'Calories Burned (cal)',
  `payment` float NOT NULL DEFAULT 0 COMMENT 'Ammount to pay (in dollars)',
  `account_id` int(11) NOT NULL,
  `e_vehicle_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ride`
--

INSERT INTO `ride` (`id`, `route_distance`, `start_time`, `end_time`, `time_rented`, `calories`, `payment`, `account_id`, `e_vehicle_id`) VALUES
(1, 33.3, '2022-01-19 09:41:56', NULL, '2022-01-16 00:00:00', 353, 5.2, 2, 3),
(2, 55, '2022-01-19 09:41:56', NULL, '2022-01-16 21:32:13', 656, 43, 3, 7),
(3, 54, '2022-01-19 10:28:04', '2022-01-21 14:56:01', '22 minutes', 33, 32, 3, 5),
(16, 5, '2022-01-21 22:09:29', '2022-01-22 22:53:46', '44 minutes', 33, 43, 3, 6),
(18, 35.2, '2022-01-21 22:11:43', '2022-01-21 23:05:58', '54 minutes', 378, 68, 3, 6);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bank_card`
--
ALTER TABLE `bank_card`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_AccountId` (`account_id`);

--
-- Indexes for table `e_vehicle`
--
ALTER TABLE `e_vehicle`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ride`
--
ALTER TABLE `ride`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_Ride_eVehicleID` (`e_vehicle_id`) USING BTREE,
  ADD KEY `FK_Ride_AccountID` (`account_id`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account`
--
ALTER TABLE `account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `bank_card`
--
ALTER TABLE `bank_card`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `e_vehicle`
--
ALTER TABLE `e_vehicle`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `ride`
--
ALTER TABLE `ride`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `bank_card`
--
ALTER TABLE `bank_card`
  ADD CONSTRAINT `FK_AccountId` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`);

--
-- Constraints for table `ride`
--
ALTER TABLE `ride`
  ADD CONSTRAINT `FK_Account_MeasurementID` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`),
  ADD CONSTRAINT `FK_Measuerement_eVehicleID` FOREIGN KEY (`e_vehicle_id`) REFERENCES `e_vehicle` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
