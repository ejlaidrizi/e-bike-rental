# E-Bike Rental Project

___

This project is an example of a RESTful API  developed using Spring Framework and Spring Boot.


## Table of Contents
* [General Info](#general-information)
* [Technologies Used](#technologies-used)
* [Setup](#setup)
* [Dependencies](#dependencies)
* [Endpoints](#endpoints)
* [Contributing](#contributing)
* [Acknowledgements](#acknowledgements)

___

## General Information
The main idea behind this project was to provide a simple RESTful API that will help 'mobile-app developers'
to build a good app that will enable users to temporarily rent e-Bikes or e-Scooter at any time to
get them across town or campus, by scanning the unique QR code embedded in each electric vehicle.

___

## Technologies used
The leading technologies used:
- [Spring Initializr](https://start.spring.io/) - for generating the project from scratch using Maven
- XAMPP - software server
- MySQL Server - i.e, phpMyAdmin for the database
- POSTMAN - for API testing

___

## Setup

### Dependencies
* **Spring Web**
* **Spring Data JPA**
* **MySQL Connector Java**
* **JUnit** (4.13.2)


For building and running the application you need:

* [JDK 16](https://www.oracle.com/java/technologies/javase/jdk16-archive-downloads.html) or higher
* [Maven 3.6.3](https://maven.apache.org/download.cgi) or higher
* [Spring Boot 2.6.3](https://spring.io/blog/2022/01/20/spring-boot-2-6-3-is-now-available)
* **DDL script** for creating the database
  * [ebikerental](database/ebikerental.sql)
* **IntelliJ IDE** (preferable)

You can run this Spring Boot application from your IDE as a simple Java application, by either 
downloading the source code from _gitlab_:
* [E-Bike Rental](https://gitlab.com/ejlaidrizi/e-bike-rental)

or cloning it using **SSH** or **HTTPS**:
```sh
git clone https://gitlab.com/ejlaidrizi/e-bike-rental.git
```

After having it all setup, you can start the **Apache** and **MySQL Server** using XAMPP control panel, execute the 
DDL script in _phpMyAdmin_ and configure the connection to the database accordingly in _application.properties_ file.
In turn, you can freely hit run **E-BikeRentalApplication** in your IDE and test the API endpoints using POSTMAN or 
whichever software you are more comfortable with.



## Endpoints
___

📑 **NOTE:**<br />
- By default, in the _application.properties_ file, the Tomcat server is configured to start on port **9090**. <br /> <br />

The available endpoints for each controller:

### Account Controller

* **GET** Request: <br />

In order to get the list of all available accounts:
```
http://localhost:9090/accounts

@GetMapping("/accounts")
```

To find an account by specifying its ID in URL path:
```
http://localhost:9090/account/8

@GetMapping("/account/{id}")
```
* Example: <br />

![](examples/getAccountById.png)

<br />

* **POST** Request: <br />


To create an account by specifying its body in a POST Http request in JSON format:
```
http://localhost:9090/account/create

@PostMapping("/account/create")
```

<br />

* **PUT** Request: <br />


To update an account by specifying its ID in the URL path and body in a POST Http request in JSON format:
```
http://localhost:9090/account/update/8

@PutMapping("/account/update/{id}")
```

<br />

* **DELETE** Request: <br />


To delete an account and all its related data by specifying the id in URL path:
```
http://localhost:9090/account/delete/8

@DeleteMapping("/account/delete/{id}")
```
**NOTE:** This method even though it's implemented, upon hitting this endpoint you will receive a foreign key related error.
The reason is that in the database the restriction ON UPDATE/DELETE CASCADE it's not added, in order to be able to keep all the data of related tables in the database. <br />
**Additional feature to be added in the future.

To return the name of a deleted account by specifying the id in URL path:
```
http://localhost:9090/account/name/8

@DeleteMapping("/account/name/{id}")
```

___

### BankCard Controller

* **GET** Request: <br />

In order to get the list of all Bank cards:
```
http://localhost:9090/bankcards

@GetMapping("/bankcards")
```

To find a Bank card by specifying its ID in URL path:
```
http://localhost:9090/bankcard/8

@GetMapping("/bankcard/{id}")
```

To find the available Bank card(s) of an account(owner) by specifying its ID in URL path:
```
http://localhost:9090/bankcards/account/8

@GetMapping("/bankcards/account/{id}")
```

To find the current available balance of a Bank card by specifying its ID in URL path:
```
http://localhost:9090/bankcard/balance/8

@GetMapping("/bankcard/balance/{id}")
``` 
<br />

* **POST** Request: <br />


To create a new Bank card for an account by specifying its body in a POST Http request in JSON format and the account ID in URL path:
```
http://localhost:9090/bankcard/create/5

@PostMapping("/bankcard/create/{acc_id}")
```

To choose among Bank cards the default one for an account by specifying its ID and the account ID in URL path:
```
http://localhost:9090/bankcard/2/5

@PostMapping("/bankcard/{id}/{acc_id}")
```

<br />

* **PUT** Request: <br />


To update a Bank card by specifying its ID in the URL path and body in a POST Http request in JSON format:
```
http://localhost:9090/bankcard/update/3

@PutMapping("/bankcard/update/{id}")
```

* Example:

![](examples/updateBankcardById.png)

<br />

* **DELETE** Request: <br />


To delete a Bank card and all its related data by specifying the id in URL path:
```
http://localhost:9090/bankcard/delete/8

@DeleteMapping("/bankcard/delete/{id}")
```

___

### E-Vehicle Controller

In contrast to the other endpoints, here it's been used the **@RequestMapping** annotation, so that
all of available endpoints for this controller will have ".../**vehicle**" as a requested URL 'root' path.

* **GET** Request: <br />

In order to get the list of all available electric vehicles:
```
http://localhost:9090/vehicle

@GetMapping("/vehicle")
```

To find an E-Vehicle by specifying its ID as a requested parameter:
```
http://localhost:9090/vehicle/

@GetMapping("/vehicle/")
```

To find an E-Vehicle by QR code by specifying as a requested parameter:
```
http://localhost:9090/vehicle/100266

@GetMapping("/vehicle/{QR}")
```

To find the current status of an E-Vehicle by specifying its QR code in URL path:
```
http://localhost:9090/vehicle/status/100266

@GetMapping("/vehicle/status/{QR}")
``` 
* Example:

![](examples/getStatusByQRCode.png)

To find the list of E-Vehicles by their current status:
```
http://localhost:9090/vehicle/100266

@GetMapping("/vehicle/{status}")
``` 

<br />

* **POST** Request: <br />


To add a new E-Vehicle by specifying its body in a POST Http request in JSON format:
```
http://localhost:9090/vehicle/insert

@PostMapping("/vehicle/insert")
```

<br />

* **PUT** Request: <br />


To update an E-Vehicle by specifying its ID in the URL path and body in a POST Http request in JSON format:
```
http://localhost:9090/vehicle/update/8

@PutMapping("/vehicle/update/{id}")
```

<br />

* **DELETE** Request: <br />


To delete an E-Vehicle and all its related data by specifying the id in URL path:
```
http://localhost:9090/vehicle/delete/8

@DeleteMapping("/vehicle/delete/{id}")
```

___

### Ride Controller

* **GET** Request: <br />

In order to get the details of all accomplished rides:
```
http://localhost:9090/rides

@GetMapping("/rides")
```

To find the rides a particular account/user has accomplished by specifying his/her ID in URL path:
```
http://localhost:9090/rides/account/8

@GetMapping("/rides/account/{id}")
```

To find the rides a particular account/user has accomplished by specifying his/her full name in URL path:
```
http://localhost:9090/rides/account/Monique Williams

@GetMapping("/rides/account/{fullname}")
```

To scan QR Code of an E-Vehicle, user can rent an E-Vehicle if it's not rented, and cancel the ride if it's his ride by
specifying the needed details as URL path variables:
```
@GetMapping("/rentorcancel/ride/{QR}/{acc_id}")
```
or
```
@GetMapping("/rentorcancel/ride/{QR}/{acc_id}/{ride_id}/{route_distance}/{calories}/{location}")
```

<br />

* **POST** Request: <br />


To rent or cancel an E-Vehicle, by scanning the QR code specifying its body in a POST Http request in JSON format:
```
http://localhost:9090/rentride

@PostMapping("/rentorcancel/ride")
```

* Example:

![](examples/rentEBikeByPOSTReq.png)

___


## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

___

## Acknowledgements

- This project idea was primarily based on [Lime](https://www.li.me/en-us/home).


### Team

Feel free to contact us!
* Suhejla Idrizi   [si28724@seeu.edu.mk]()
* Romir Agolli     [ra28681@seeu.edu.mk]()





