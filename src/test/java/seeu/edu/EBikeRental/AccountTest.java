package seeu.edu.EBikeRental;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import seeu.edu.EBikeRental.exceptions.InvalidInputsException;
import seeu.edu.EBikeRental.pojo.entity.Account;
import seeu.edu.EBikeRental.pojo.input.AccountInput;
import seeu.edu.EBikeRental.service.interfaces.AccountService;

import static org.junit.Assert.assertEquals;

@SpringBootTest
@RunWith(SpringRunner.class)
public class AccountTest {
    private final static String FULL_NAME = "Test User";
    private final static String EMAIL = "testuser@gmail.com";
    private final static String WRONG_EMAIL = "testuser.gmail";
    private final static String PHONE_NUMBER = "+389 70 123 456";

    private AccountInput accountInput;

    @Autowired
    private AccountService accountService;

    @Before
    public void setUp() {
        accountInput = new AccountInput(FULL_NAME, EMAIL, PHONE_NUMBER);
    }

    @Test
    public void testAccountInputConstructor() {
        assertEquals(FULL_NAME, accountInput.getFullName());
        assertEquals(EMAIL, accountInput.getEmail());
        assertEquals(PHONE_NUMBER, accountInput.getPhoneNumber());
    }

    @Test
    public void testCreateAccountNormalFlow() throws InvalidInputsException {
        Account account = accountService.createAccount(accountInput);

        assertEquals(accountInput.getFullName(), account.getFullName());
        assertEquals(accountInput.getEmail(), account.getEmail());
        assertEquals(accountInput.getPhoneNumber(), account.getPhoneNumber());
    }

    @Test(expected = InvalidInputsException.class)
    public void testCreateAccountWrongFlow() throws InvalidInputsException {
        accountInput.setEmail(WRONG_EMAIL);
        Account account = accountService.createAccount(accountInput);

        assertEquals(accountInput.getFullName(), account.getFullName());
        assertEquals(accountInput.getEmail(), account.getEmail());
        assertEquals(accountInput.getPhoneNumber(), account.getPhoneNumber());
    }
}
