package seeu.edu.EBikeRental.service;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import seeu.edu.EBikeRental.pojo.entity.Account;
import seeu.edu.EBikeRental.pojo.entity.BankCard;
import seeu.edu.EBikeRental.pojo.entity.EVehicle;
import seeu.edu.EBikeRental.pojo.entity.Ride;
import seeu.edu.EBikeRental.pojo.enums.BankCardType;
import seeu.edu.EBikeRental.pojo.enums.ERide;
import seeu.edu.EBikeRental.repository.BankCardRepository;
import seeu.edu.EBikeRental.repository.EVehicleRepository;
import seeu.edu.EBikeRental.repository.RideRepository;
import seeu.edu.EBikeRental.service.interfaces.RideService;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
class RideServiceImplTest {

    @MockBean
    private EVehicleRepository eVehicleRepository;

    @MockBean
    private RideRepository rideRepository;

    @MockBean
    private BankCardRepository bankCardRepository;

    @Autowired
    private RideService rideService;

    @Test
    public void testRentRide() {
        String qrCode = "QR123";
        Integer accountId = 1;
        Integer eVehicleId = 123;

        EVehicle mockedVehicle = new EVehicle();
        mockedVehicle.setId(eVehicleId);
        mockedVehicle.setStatus(ERide.vehicleStatus.FREE.name());

        when(eVehicleRepository.findEVehicleByQRCode(qrCode)).thenReturn(mockedVehicle);
        when(rideRepository.existsById(accountId)).thenReturn(false);

        String result = rideService.rentOrCancelRide(qrCode, accountId, Optional.empty(), Optional.empty(),
                Optional.empty(), Optional.empty());

        assertEquals("Vehicle successfully rented!", result);
    }

    @Test
    void testCancelRide() {
        Integer eVehicleId = 456;
        Integer accountId = 23;
        String qrCode = "QR123";
        String location = "SomeLocation";
        Float balance = 5600.25f;

        EVehicle mockedVehicle = new EVehicle();
        mockedVehicle.setId(eVehicleId);
        mockedVehicle.setQRCode("QR123");
        mockedVehicle.setLocation(location);
        mockedVehicle.seteType("E_BIKE");
        mockedVehicle.setMinutePrice(30.0f);
        mockedVehicle.setStatus(ERide.vehicleStatus.RENTED.name());

        // Mocking eVehicleRepository behavior
        when(eVehicleRepository.findEVehicleByQRCode(qrCode)).thenReturn(mockedVehicle);

        // Mocking eBankCardRepository behaviour
        when(bankCardRepository.getDefaultBankCardAmount(accountId)).thenReturn(balance);

        // Mocking rideRepository behavior
        Ride mockedRide = new Ride();
        mockedRide.setId(123);
        mockedRide.setCalories(150.0f);
        mockedRide.setRouteDistance(10.0f);
        mockedRide.setStartTime(LocalDateTime.now());
        mockedRide.setEndTime(null);  // Simulate an ongoing ride
        mockedRide.setEVehicle(mockedVehicle);
        Account account = new Account();
        account.setId(accountId);
        account.setEmail("ejla-test");
        account.setBankCards(Collections.singleton(new BankCard(1,
                BankCardType.VISA.name(),
                BankCardType.VISA.name(),
                "4242 4242 4242 4242",
                "S Idr",
                147,
                balance,
                "10/25",
                account)));
        mockedRide.setAccount(account);
        when(rideRepository.getRideBy(accountId, eVehicleId)).thenReturn(mockedRide);

        String result = rideService.rentOrCancelRide(qrCode, accountId, Optional.empty(),
                Optional.of(mockedRide.getRouteDistance()), Optional.of(mockedRide.getCalories()), Optional.of(location));

        assertEquals("Vehicle successfully cancelled!", result);
    }

    @Test
    public void testFailRentOrCancelRide() {
        String qrCode = "QR123";
        Integer accountId = 1;
        Integer rideId = 123;
        Float routeDistance = 10.0f;
        Float calories = 150.0f;
        String location = "SomeLocation";
        Integer eVehicleId = 456;

        EVehicle mockedVehicle = new EVehicle();
        mockedVehicle.setId(eVehicleId);
        mockedVehicle.setStatus(ERide.vehicleStatus.RENTED.name());

        when(eVehicleRepository.findEVehicleByQRCode(qrCode)).thenReturn(mockedVehicle);
        when(rideRepository.existsById(accountId)).thenReturn(false);
        when(rideRepository.getEndTime(Optional.of(rideId))).thenReturn(null);


        String result = rideService.rentOrCancelRide(qrCode, accountId, Optional.of(rideId),
                Optional.of(routeDistance), Optional.of(calories), Optional.of(location));

        assertEquals("Failed to rent/cancel!", result);
    }

    @Test
    public void testVehicleNotFoundToRent() {
        String qrCode = "QR123";
        Integer accountId = 1;
        Integer rideId = 123;
        Float routeDistance = 10.0f;
        Float calories = 150.0f;
        String location = "SomeLocation";

        when(eVehicleRepository.findEVehicleByQRCode(qrCode)).thenReturn(null);

        String result = rideService.rentOrCancelRide(qrCode, accountId, Optional.of(rideId),
                Optional.of(routeDistance), Optional.of(calories), Optional.of(location));

        assertEquals("No vehicle found!", result);
    }
}