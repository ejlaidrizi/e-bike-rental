package seeu.edu.EBikeRental.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import seeu.edu.EBikeRental.pojo.entity.BankCard;
import seeu.edu.EBikeRental.sql.BankCardSQL;

import java.util.Set;

@Repository
public interface BankCardRepository extends CrudRepository<BankCard, Integer> {

    Set<BankCard> findAll();

    Set<BankCard> findBankCardsByAccountId(Integer id);

    BankCard findBankCardById(Integer id);


    @Query(
            value = BankCardSQL.GET_BALANCE, nativeQuery = true)
    /*@Query(
            "SELECT balance FROM BankCard b WHERE b.id = :id "
    )*/
    Float findBalanceOfCardById(Integer id);

    @Modifying
    @Transactional
    @Query(value = BankCardSQL.UPDATE_BALANCE, nativeQuery = true)
    void updateMoney(@Param("id") Integer id, @Param("balance") Float balance);

    @Query(value = BankCardSQL.GET_DEFAULT, nativeQuery = true)
    Float getDefaultBankCardAmount(Integer accountId);

    @Query(value = BankCardSQL.GET_ID_DEFAULT, nativeQuery = true)
    Integer getIdOfDefaultBankCard(Integer accountId);

    @Modifying
    @Transactional
    @Query(value = BankCardSQL.UPDATE_DEFAULT_TYPE, nativeQuery = true)
    void updateBankCardDefaultStatus(Integer id, String defaultType);

    @Modifying
    @Transactional
    @Query(value = BankCardSQL.INSERT, nativeQuery = true)
    void insertABankCard(String type, String cardNumber, String cardHolderName, Integer cvv, Float balance,
                         String expirationDate, Integer accId);
}
