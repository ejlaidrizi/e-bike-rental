package seeu.edu.EBikeRental.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import seeu.edu.EBikeRental.pojo.entity.Account;
import seeu.edu.EBikeRental.sql.AccountSQL;

import java.util.Set;

@Repository
public interface AccountRepository extends CrudRepository<Account, Integer> {

    Set<Account> findAll();                             //returns all accounts

    Account findAccountById(Integer id);                //returns an account

    @Query(value = AccountSQL.FIND_ID_BY_NAME, nativeQuery = true)
    Integer findAccountIdByFullName(@Param("fullname") String fullName);     //gets id of Acc by FullName
}
