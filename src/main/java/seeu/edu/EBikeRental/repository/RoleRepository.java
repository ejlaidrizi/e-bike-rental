package seeu.edu.EBikeRental.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import seeu.edu.EBikeRental.pojo.entity.Role;
import seeu.edu.EBikeRental.pojo.enums.ERole;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(ERole name);
}
