package seeu.edu.EBikeRental.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import seeu.edu.EBikeRental.pojo.entity.Ride;
import seeu.edu.EBikeRental.sql.RideSQL;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.Set;

@Repository
public interface RideRepository extends CrudRepository<Ride, Integer> {

    Set<Ride> findAll();

    Set<Ride> findRidesByAccountId(Integer id);

    boolean existsById(Optional<Integer> id);

    @Modifying
    @Transactional
    @Query(value = RideSQL.INSERT, nativeQuery = true)
    void rentRide(Integer accountId, Integer eVehicleId);

    @Modifying
    @Transactional
    @Query(value = RideSQL.CANCEL_RIDE, nativeQuery = true)
    void cancelRide(Integer rideId, Optional<Float> routeDistance, LocalDateTime endTime, Long rentTime,
                    Optional<Float> calories, Float payment);

    @Query(value = RideSQL.GET_START_TIME, nativeQuery = true)
    LocalDateTime getStartTime(Optional<Integer> id);

    @Query(value = RideSQL.GET_END_TIME, nativeQuery = true)
    String getEndTime(Optional<Integer> id);

    @Query(value = RideSQL.GET_RIDE_BY, nativeQuery = true)
    Ride getRideBy(@Param("account_id") Integer accountId, @Param("vehicle_id") Integer vehicleId);
}
