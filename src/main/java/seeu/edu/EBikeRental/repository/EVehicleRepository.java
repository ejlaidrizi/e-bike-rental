package seeu.edu.EBikeRental.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import seeu.edu.EBikeRental.pojo.entity.EVehicle;
import seeu.edu.EBikeRental.sql.VehicleSQL;

import java.util.Optional;
import java.util.Set;

@Repository
public interface EVehicleRepository extends CrudRepository<EVehicle, Integer> {

    Set<EVehicle> findAll();                               //gets All Bike details

    Set<EVehicle> findEVehiclesByStatus(String status);   //gets Bikes by Statuses

    EVehicle findEVehicleById(Integer id);                //gets Bike details by id

    EVehicle findEVehicleByQRCode(String QRCode);         //gets Bike details by QRcode

    @Query(value = VehicleSQL.GET_STATUS_BY_QR, nativeQuery = true)
    String findVehicleStatusByQRCode(String QRCode);        //get Status detail

    @Query(value = VehicleSQL.GET_TYPE_BY_QR, nativeQuery = true)
    String findVehicleTypeByQRCode(String QRCode);

    @Query(value = VehicleSQL.GET_ID_BY_QR, nativeQuery = true)
    Integer findVehicleIdByQRCode(String QRCode);           //get id of bike

    @Query(value = VehicleSQL.GET_PRICE_PER_MIN_BY_QR, nativeQuery = true)
    Float getVehiclesMinutePriceByQRCode(String QRCode);   //get m_price of bike

    @Modifying
    @Transactional
    @Query(value = VehicleSQL.UPDATE,
            nativeQuery = true)
    void changeStatusAndLocation(@Param("veh_id") Integer eVehicleId, @Param("status") String status,
                                 @Param("location") Optional<String> location);

}
