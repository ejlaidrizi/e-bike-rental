package seeu.edu.EBikeRental.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import seeu.edu.EBikeRental.exceptions.BadRequestException;
import seeu.edu.EBikeRental.pojo.entity.EVehicle;
import seeu.edu.EBikeRental.pojo.input.EVehicleInput;
import seeu.edu.EBikeRental.service.interfaces.EVehicleService;

import java.util.Set;

@RestController
@RequestMapping("/api/vehicles")
public class EVehicleController {

    @Autowired
    private EVehicleService eVehicleService;

    @CrossOrigin
    @GetMapping
    @PreAuthorize("hasRole('ADMIN')")
    public Set<EVehicle> getAllVehicles() {
        return eVehicleService.findAll();
    }

    @CrossOrigin
    @GetMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    public EVehicle getVehicleById(@PathVariable Integer id) {
        return eVehicleService.findEVehicleById(id);
    }

    @GetMapping("/qr")
    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    public EVehicle getVehicleByQRCode(@RequestBody String QRCode) {
        return eVehicleService.findEVehicleByQRCode(QRCode);
    }

    @GetMapping("/{qr}")
    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    public String getStatusByQRCode(@PathVariable String qr) {
        return eVehicleService.findVehicleStatusByQRCode(qr);
    }

    @GetMapping("/{status}")
    public Set<EVehicle> findEVehiclesByStatus(@PathVariable String status) {
        return eVehicleService.findEVehiclesByStatus(status);
    }

    @PostMapping()
    @PreAuthorize("hasRole('ADMIN')")
    public EVehicle insertVehicle(@RequestBody EVehicleInput eVehicleInput) {
        String msg = eVehicleInput.validate();
        if (msg != null) {
            throw new BadRequestException(msg);
        }
        return eVehicleService.insertVehicle(eVehicleInput);
    }

    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasRole('ADMIN')")
    public EVehicle updateVehicleData(@PathVariable Integer id,
                                      @RequestBody EVehicleInput eVehicleInput) {
        String msg = eVehicleInput.validate();
        if (msg != null) {
            throw new BadRequestException(msg);
        }
        EVehicle eVehicle = eVehicleService.findEVehicleById(id);

        return eVehicleService.updateVehicle(eVehicle, eVehicleInput);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public boolean deleteVehicleData(@PathVariable Integer id) {
        eVehicleService.delete(id);
        return true;
    }
}
