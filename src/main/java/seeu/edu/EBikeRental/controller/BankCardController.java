package seeu.edu.EBikeRental.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import seeu.edu.EBikeRental.pojo.entity.BankCard;
import seeu.edu.EBikeRental.pojo.input.BankCardInput;
import seeu.edu.EBikeRental.service.interfaces.BankCardService;

import java.util.Set;

@RestController
@RequestMapping("/api/bank-cards")
public class BankCardController {

    @Autowired
    BankCardService bankCardService;

    @CrossOrigin
    @GetMapping
    @PreAuthorize("hasRole('ADMIN')")
    public Set<BankCard> getAllBankCards() {
        return bankCardService.findAll();
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    public BankCard getBankCardById(@PathVariable Integer id) {
        return bankCardService.findBankCardById(id);
    }

    @GetMapping("/{id}/balance")
    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    public Float findCardBalanceById(@PathVariable Integer id) {
        return bankCardService.findCardBalanceById(id);
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    public BankCard updateBankCard(@PathVariable Integer id,
                                   @RequestBody BankCardInput bankCardInput) {
        BankCard bankCard = bankCardService.findBankCardById(id);
        return bankCardService.updateAccount(bankCard, bankCardInput);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public boolean deleteBankCardAccount(@PathVariable Integer id) {
        bankCardService.delete(id);
        return true;
    }
}
