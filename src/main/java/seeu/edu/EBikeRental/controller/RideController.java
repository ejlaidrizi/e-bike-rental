package seeu.edu.EBikeRental.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import seeu.edu.EBikeRental.pojo.entity.Ride;
import seeu.edu.EBikeRental.pojo.input.RentRideInput;
import seeu.edu.EBikeRental.service.interfaces.RideService;

import java.util.Optional;
import java.util.Set;

@RestController
@RequestMapping("/api/rides")
public class RideController {

    @Autowired
    private RideService rideService;

    @CrossOrigin
    @GetMapping()
    @PreAuthorize("hasRole('ADMIN')")
    public Set<Ride> findAllRentedRidesDetails() {
        return rideService.findAll();
    }

    @CrossOrigin
    @GetMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    public Set<Ride> findAllRentedRidesOfAnAccount(@PathVariable Integer id) {
        return rideService.findRidesByAccountId(id);
    }

    @CrossOrigin
    @GetMapping("/name/{fullname}")
    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    public Set<Ride> findAllRentedRidesOfAnAccountByFullName(@PathVariable(name = "fullname") String fullName) {
        return rideService.findRidesByAccountFullName(fullName);
    }

    @CrossOrigin
    @GetMapping(value = {"/rent-ride/{qr}/{acc_id}",
            "/rent-ride/{qr}/{acc_id}/{ride_id}/{route_distance}/{calories}/{location}"})
    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    public String scanningQR(@PathVariable String qr,
                             @PathVariable(name = "acc_id") Integer accId,
                             @PathVariable(name = "ride_id") Optional<Integer> rideId,
                             @PathVariable(name = "route_distance") Optional<Float> routeDistance,
                             @PathVariable Optional<Float> calories,
                             @PathVariable Optional<String> location) {
        return rideService.rentOrCancelRide(qr, accId, rideId, routeDistance, calories, location);
    }

    @PostMapping("/rent-ride")
    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    public String scanningQRByBody(@RequestBody RentRideInput rentRideInput) {
        return rideService.rentOrCancelRide(rentRideInput);
    }
}
