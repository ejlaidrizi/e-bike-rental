package seeu.edu.EBikeRental.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import seeu.edu.EBikeRental.exceptions.BadRequestException;
import seeu.edu.EBikeRental.pojo.entity.Account;
import seeu.edu.EBikeRental.pojo.entity.BankCard;
import seeu.edu.EBikeRental.pojo.input.AccountInput;
import seeu.edu.EBikeRental.pojo.input.BankCardInput;
import seeu.edu.EBikeRental.service.interfaces.AccountService;
import seeu.edu.EBikeRental.service.interfaces.BankCardService;

import java.util.Set;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/accounts")
public class AccountController {

    @Autowired
    private AccountService accountService;
    @Autowired
    private BankCardService bankCardService;

    @CrossOrigin
    @GetMapping
    @PreAuthorize("hasRole('ADMIN')")
    public Set<Account> getAllAccounts() {
        return accountService.findAll();
    }

    @CrossOrigin
    @GetMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    public Account getAccountById(@PathVariable Integer id) {
        return accountService.findAccountById(id);
    }

    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    public Account updateAccount(@PathVariable Integer id,
                                 @RequestBody AccountInput accountInput) {
        String msg = accountInput.validate();
        if (msg != null) {
            throw new BadRequestException(msg);
        }
        Account account = accountService.findAccountById(id);
        if (account == null) {
            throw new BadRequestException("Account not found!");
        }
        return accountService.updateAccount(accountInput);
    }

    @CrossOrigin
    @DeleteMapping("/{id}")//TODO:Delete when u have foreign keys implemented
    @PreAuthorize("hasRole('ADMIN')")
    public boolean deleteAccount(@PathVariable Integer id) {
        accountService.deleteAccount(id);
        return true;
    }

    //OR
    @DeleteMapping("/name/{id}")
    public String returnFullName(@PathVariable Integer id) {
        return accountService.returnDeletedAccountFullName(id);
    }

    @CrossOrigin
    @GetMapping("/{id}/bank-cards")
    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    public Set<BankCard> getBankCardsByAccountId(@PathVariable Integer id) {
        return bankCardService.findBankCardsByAccountId(id);
    }

    @PostMapping("/{id}/bank-cards")
    @PreAuthorize("hasRole('ADMIN')")
    public boolean createBankCard(@RequestBody BankCardInput bankCardInput,
                                  @PathVariable Integer id) {
        return bankCardService.createBankCardForAccount(bankCardInput, id);
    }

    @PutMapping("/{id}/bank-cards/{bank_card_id}/default")
    @PreAuthorize("hasRole('ADMIN') or hasRole('USER')")
    public String chooseDefaultCard(@PathVariable Integer id,
                                    @PathVariable(name = "bank_card_id") Integer bankCardId) {
        bankCardService.chooseDefaultCard(bankCardId, id);
        return "Bank Card was chosen as default!";
    }
}
