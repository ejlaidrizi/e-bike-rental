package seeu.edu.EBikeRental.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import seeu.edu.EBikeRental.pojo.entity.EVehicle;
import seeu.edu.EBikeRental.pojo.entity.Ride;
import seeu.edu.EBikeRental.pojo.enums.ERide;
import seeu.edu.EBikeRental.pojo.input.RentRideInput;
import seeu.edu.EBikeRental.repository.AccountRepository;
import seeu.edu.EBikeRental.repository.BankCardRepository;
import seeu.edu.EBikeRental.repository.EVehicleRepository;
import seeu.edu.EBikeRental.repository.RideRepository;
import seeu.edu.EBikeRental.service.interfaces.RideService;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Optional;
import java.util.Set;

@Service
public class RideServiceImpl implements RideService {

    public static final Float E_SCOOTER_FEE = 0.5f;
    public static final Float E_BIKE_FEE = 0.3f;

    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private RideRepository rideRepository;
    @Autowired
    private EVehicleRepository eVehicleRepository;
    @Autowired
    private BankCardRepository bankCardRepository;

    @Override
    public Set<Ride> findAll() {
        return rideRepository.findAll();
    }

    @Override
    public Set<Ride> findRidesByAccountId(Integer id) {
        return rideRepository.findRidesByAccountId(id);
    }

    @Override
    public Set<Ride> findRidesByAccountFullName(String fullName) {
        Integer id = accountRepository.findAccountIdByFullName(fullName);
        return rideRepository.findRidesByAccountId(id);
    }

    @Override
    public String rentOrCancelRide(String qr, Integer accountId, Optional<Integer> rideId, Optional<Float> routeDistance,
                                   Optional<Float> calories, Optional<String> location) {
        EVehicle eVehicle = eVehicleRepository.findEVehicleByQRCode(qr);
        if (eVehicle == null) {
            return "No vehicle found!";
        }

        if (eVehicle.getStatus().equals(ERide.vehicleStatus.FREE.name())) {
            return rentRide(accountId, eVehicle.getId());
        }

        if (eVehicle.getStatus().equals(ERide.vehicleStatus.RENTED.name())) {
            Ride ride = rideRepository.getRideBy(accountId, eVehicle.getId());
            if (ride != null && ride.getEndTime() == null) {
                return cancelRide(eVehicle, ride, calories, location);
            }
        }

        return "Failed to rent/cancel!";
    }

    private String cancelRide(EVehicle eVehicle, Ride ride, Optional<Float> calories, Optional<String> location) {
        LocalDateTime endTime = LocalDateTime.now();
        Long minutesRented = timeRented(ride, endTime);
        Float payment = withdraw(minutesRented, eVehicle.getMinutePrice(), ride.getAccount().getId(),
                getStartFee(eVehicle.geteType()));

        rideRepository.cancelRide(ride.getId(), Optional.of(ride.getRouteDistance()), endTime, minutesRented, calories, payment);
        eVehicleRepository.changeStatusAndLocation(ride.getEVehicle().getId(), ERide.vehicleStatus.FREE.name(), location);

        return "Vehicle successfully cancelled!";
    }

    private String rentRide(Integer accountId, Integer eVehicleId) {
        rideRepository.rentRide(accountId, eVehicleId);
        eVehicleRepository.changeStatusAndLocation(eVehicleId, ERide.vehicleStatus.RENTED.name(), Optional.of("ON_ROAD"));

        return "Vehicle successfully rented!";
    }

    @Override
    public String rentOrCancelRide(RentRideInput rentRideInput) {
        String qrCode = rentRideInput.getQrCode();
        Integer accountId = rentRideInput.getAccountId();
        Optional<Integer> rideId = rentRideInput.getRideId();
        Optional<Float> routeDistance = rentRideInput.getRouteDistance();
        Optional<Float> calories = rentRideInput.getCalories();
        Optional<String> location = rentRideInput.getLocation();
        return rentOrCancelRide(qrCode, accountId, rideId, routeDistance, calories, location);
    }

    private Float getStartFee(String eType) {
        float startFee = 0f;
        if (eType.equals(ERide.vehicleType.E_SCOOTER.name())) {
            startFee = E_SCOOTER_FEE;
        } else if (eType.equals(ERide.vehicleType.E_BIKE.name())) {
            startFee = E_BIKE_FEE;
        }
        return startFee;
    }

    private Long timeRented(Ride ride, LocalDateTime endTime) {
        LocalDateTime startTime = ride.getStartTime();
        return startTime.until(endTime, ChronoUnit.MINUTES);
    }

    private Float withdraw(Long minutesRented, Float minutePrice, Integer accountId, Float startFee) {
        Float balance = bankCardRepository.getDefaultBankCardAmount(accountId);
        float amount = (minutePrice * minutesRented) + startFee;
        balance -= amount;
        bankCardRepository.updateMoney(accountId, balance);
        return amount;
    }
}
