package seeu.edu.EBikeRental.service.interfaces;

import seeu.edu.EBikeRental.pojo.entity.EVehicle;
import seeu.edu.EBikeRental.pojo.input.EVehicleInput;

import java.util.Set;

public interface EVehicleService {

    Set<EVehicle> findAll();

    EVehicle findEVehicleById(Integer id);

    EVehicle findEVehicleByQRCode(String QRCode);

    String findVehicleStatusByQRCode(String QRCode);

    Set<EVehicle> findEVehiclesByStatus(String status);

    EVehicle insertVehicle(EVehicleInput eVehicleInput);

    EVehicle updateVehicle(EVehicle eVehicle, EVehicleInput eVehicleInput);

    void delete(Integer id);
}
