package seeu.edu.EBikeRental.service.interfaces;

import seeu.edu.EBikeRental.pojo.entity.Ride;
import seeu.edu.EBikeRental.pojo.input.RentRideInput;

import java.util.Optional;
import java.util.Set;

public interface RideService {

    Set<Ride> findAll();

    Set<Ride> findRidesByAccountId(Integer id);  //gets all the rides of the User by Acc id

    Set<Ride> findRidesByAccountFullName(String fullName);       //gets all the rides of the USer by Acc fullName

    String rentOrCancelRide(String qr, Integer accountId, Optional<Integer> rideId, Optional<Float> routeDistance,
                            Optional<Float> calories, Optional<String> location);

    String rentOrCancelRide(RentRideInput rentRideInput);
}
