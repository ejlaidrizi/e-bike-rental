package seeu.edu.EBikeRental.service.interfaces;

import seeu.edu.EBikeRental.pojo.entity.Account;
import seeu.edu.EBikeRental.pojo.input.AccountInput;

import java.util.Set;

public interface AccountService {

    Set<Account> findAll();

    Account findAccountById(Integer id);

    Account createAccount(AccountInput accountInput);

    Account updateAccount(AccountInput accountInput);

    void deleteAccount(Integer id);

    String returnDeletedAccountFullName(Integer id);
}
