package seeu.edu.EBikeRental.service.interfaces;

import seeu.edu.EBikeRental.pojo.entity.BankCard;
import seeu.edu.EBikeRental.pojo.input.BankCardInput;

import java.util.Set;

public interface BankCardService {

    Set<BankCard> findAll();

    Set<BankCard> findBankCardsByAccountId(Integer id);

    BankCard findBankCardById(Integer id);

    Float findCardBalanceById(Integer id);

    boolean createBankCardForAccount(BankCardInput bankCardInput, Integer accId);

    BankCard updateAccount(BankCard bankCard, BankCardInput bankCardInput);

    void chooseDefaultCard(Integer bcId, Integer accId);

    void delete(Integer id);
}
