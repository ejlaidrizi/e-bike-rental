package seeu.edu.EBikeRental.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import seeu.edu.EBikeRental.pojo.entity.BankCard;
import seeu.edu.EBikeRental.pojo.input.BankCardInput;
import seeu.edu.EBikeRental.repository.BankCardRepository;
import seeu.edu.EBikeRental.service.interfaces.BankCardService;

import java.util.Set;

import static java.util.Objects.isNull;

@Service
public class BankCardServiceImpl implements BankCardService {

    @Autowired
    private BankCardRepository bankCardRepository;

    @Override
    public Set<BankCard> findAll() {
        return bankCardRepository.findAll();
    }

    @Override
    public Set<BankCard> findBankCardsByAccountId(Integer id) {
        return bankCardRepository.findBankCardsByAccountId(id);
    }

    @Override
    public BankCard findBankCardById(Integer id) {
        return bankCardRepository.findBankCardById(id);
    }

    @Override
    public Float findCardBalanceById(Integer id) {
        return bankCardRepository.findBalanceOfCardById(id);
    }

    @Override
    public boolean createBankCardForAccount(BankCardInput bankCardInput, Integer accId) {
        String type = bankCardInput.getType().toString();
        String cardNumber = bankCardInput.getCardNumber();
        String cardHolderName = bankCardInput.getCardHolderName();
        Integer cvv = bankCardInput.getCvv();
        Float balance = bankCardInput.getBalance(); //Only because we need it, otherwise doesn't make sense
        String expirationDate = bankCardInput.getExpirationDate();

        bankCardRepository.insertABankCard(type, cardNumber, cardHolderName, cvv, balance, expirationDate, accId);
        return true;
    }

    @Override
    public BankCard updateAccount(BankCard bankCard, BankCardInput bankCardInput) {
        bankCard.setType(bankCardInput.getType().toString());
        bankCard.setCardNumber(bankCardInput.getCardNumber());
        bankCard.setCardHolderName(bankCardInput.getCardHolderName());
        bankCard.setCvv(bankCardInput.getCvv());
        bankCard.setBalance(bankCardInput.getBalance());
        bankCard.setExpirationDate(bankCardInput.getExpirationDate());

        return bankCardRepository.save(bankCard);
    }

    @Override
    public void chooseDefaultCard(Integer bcId, Integer accId) {
        Integer index = bankCardRepository.getIdOfDefaultBankCard(accId);
        if (!isNull(index)) {
            bankCardRepository.updateBankCardDefaultStatus(index, "NULL");
        }
        bankCardRepository.updateBankCardDefaultStatus(bcId, "Default");
    }

    @Override
    public void delete(Integer id) {
        bankCardRepository.deleteById(id);
    }
}
