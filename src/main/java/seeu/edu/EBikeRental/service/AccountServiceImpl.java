package seeu.edu.EBikeRental.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import seeu.edu.EBikeRental.pojo.entity.Account;
import seeu.edu.EBikeRental.pojo.input.AccountInput;
import seeu.edu.EBikeRental.repository.AccountRepository;
import seeu.edu.EBikeRental.service.interfaces.AccountService;

import java.util.Set;

@Service
public class AccountServiceImpl implements AccountService {

    @Autowired
    private AccountRepository accountRepository;

    @Override
    public Set<Account> findAll() {
        return accountRepository.findAll();
    }

    @Override
    public Account findAccountById(Integer id) {
        if (id == null) {
            return null;
        }
        return accountRepository.findAccountById(id);
    }

    @Override
    public Account createAccount(AccountInput accountInput) {
        Account account = new Account();
        account.setFullName(accountInput.getFullName());
        account.setEmail(accountInput.getEmail());
        account.setPhoneNumber(accountInput.getPhoneNumber());

        return accountRepository.save(account);
    }

    @Override
    public Account updateAccount(AccountInput accountInput) {
        return accountRepository.save(new Account(accountInput));
    }

    @Override
    public void deleteAccount(Integer id) {
        if (id == null) {
            return;
        }
        accountRepository.deleteById(id);
    }

    @Override
    public String returnDeletedAccountFullName(Integer id) {
        String fullName = accountRepository.findAccountById(id).getFullName();
        deleteAccount(id);
        return fullName;
    }
}
