package seeu.edu.EBikeRental.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import seeu.edu.EBikeRental.pojo.entity.EVehicle;
import seeu.edu.EBikeRental.pojo.enums.ERide;
import seeu.edu.EBikeRental.pojo.input.EVehicleInput;
import seeu.edu.EBikeRental.repository.EVehicleRepository;
import seeu.edu.EBikeRental.service.interfaces.EVehicleService;

import java.util.Set;

@Service
public class EVehicleServiceImpl implements EVehicleService {

    @Autowired
    private EVehicleRepository eVehicleRepository;

    @Override
    public Set<EVehicle> findAll() {
        return eVehicleRepository.findAll();
    }

    @Override
    public EVehicle findEVehicleById(Integer id) {
        return eVehicleRepository.findEVehicleById(id);
    }

    @Override
    public EVehicle findEVehicleByQRCode(String QRCode) {
        return eVehicleRepository.findEVehicleByQRCode(QRCode);
    }

    @Override
    public String findVehicleStatusByQRCode(String QRCode) {
        return eVehicleRepository.findVehicleStatusByQRCode(QRCode);
    }

    @Override
    public Set<EVehicle> findEVehiclesByStatus(String status) {
        return eVehicleRepository.findEVehiclesByStatus(status);
    }

    @Override
    public EVehicle insertVehicle(EVehicleInput eVehicleInput) {
        EVehicle eVehicle = new EVehicle();
        eVehicle.setModelName(eVehicleInput.getModelName());
        eVehicle.setBrandName(eVehicleInput.getBrandName());
        eVehicle.seteType(eVehicleInput.geteType());
        eVehicle.setMaxSpeed(eVehicleInput.getMaxSpeed());
        eVehicle.setMaxBatteryCapacity(eVehicleInput.getMaxBatteryCapacity());
        eVehicle.setCurrentBatteryCapacity(eVehicleInput.getCurrentBatteryCapacity());
        eVehicle.setRangePerCharge(eVehicleInput.getRangePerCharge());
        eVehicle.setMinutePrice(eVehicleInput.getMinutePrice());
        eVehicle.setStatus(ERide.vehicleStatus.FREE.name());
        eVehicle.setLocation(eVehicleInput.getLocation());
        eVehicle.setQRCode(eVehicleInput.getQRCode());

        return eVehicleRepository.save(eVehicle);
    }

    @Override
    public EVehicle updateVehicle(EVehicle eVehicle, EVehicleInput eVehicleInput) {
        eVehicle.setModelName(eVehicleInput.getModelName());
        eVehicle.setBrandName(eVehicleInput.getBrandName());
        eVehicle.seteType(eVehicleInput.geteType());
        eVehicle.setMaxSpeed(eVehicleInput.getMaxSpeed());
        eVehicle.setMaxBatteryCapacity(eVehicleInput.getMaxBatteryCapacity());
        eVehicle.setCurrentBatteryCapacity(eVehicleInput.getCurrentBatteryCapacity());
        eVehicle.setRangePerCharge(eVehicleInput.getRangePerCharge());
        eVehicle.setMinutePrice(eVehicleInput.getMinutePrice());
        eVehicle.setStatus(eVehicleInput.getStatus());
        eVehicle.setLocation(eVehicleInput.getLocation());
        eVehicle.setQRCode(eVehicleInput.getQRCode());

        return eVehicleRepository.save(eVehicle);
    }

    @Override
    public void delete(Integer id) {
        eVehicleRepository.deleteById(id);
    }
}
