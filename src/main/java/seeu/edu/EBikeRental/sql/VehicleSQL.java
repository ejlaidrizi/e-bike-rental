package seeu.edu.EBikeRental.sql;

public class VehicleSQL {
    public static final String GET_STATUS_BY_QR = "SELECT status FROM e_vehicles v WHERE v.QR_code = ?1";

    public static final String GET_TYPE_BY_QR = "SELECT e_type FROM e_vehicles v WHERE v.QR_code = ?1";

    public static final String GET_ID_BY_QR = "SELECT id FROM e_vehicles v WHERE v.QR_code = ?1";

    public static final String GET_PRICE_PER_MIN_BY_QR = "SELECT minute_price FROM e_vehicles v WHERE v.QR_code = ?1";

    public static final String UPDATE = "" +
            "UPDATE e_vehicles \n" +
            "SET status   = :status, \n" +
            "    location = :location \n" +
            "WHERE id = :veh_id;";
}
