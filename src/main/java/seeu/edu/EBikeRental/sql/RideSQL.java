package seeu.edu.EBikeRental.sql;

public class RideSQL {
    public static final String INSERT = "" +
            "INSERT INTO `rides`(\n" +
            "    `id`,\n" +
            "    `route_distance`,\n" +
            "    `start_time`,\n" +
            "    `end_time`,\n" +
            "    `time_rented`,\n" +
            "    `calories`,\n" +
            "    `payment`,\n" +
            "    `account_id`,\n" +
            "    `e_vehicle_id`\n" +
            ")\n" +
            "VALUES(\n" +
            "    NULL,\n" +
            "    '0',\n" +
            "    CURRENT_TIMESTAMP(), NULL, NULL, '0', '0', ?1, ?2)";

    public static final String CANCEL_RIDE = "" +
            "UPDATE\n" +
            "    `rides`\n" +
            "SET\n" +
            "    `route_distance` = ?2,\n" +
            "    `end_time` = ?3,\n" +
            "    `time_rented` = CONCAT(?4, 'minutes'),\n" +
            "    `calories` = ?5,\n" +
            "    `payment` = ?6\n" +
            "WHERE\n" +
            "    `id` = ?1";

    public static final String GET_START_TIME = "SELECT start_time FROM rides r WHERE r.id = ?1";

    public static final String GET_END_TIME = "SELECT end_time FROM rides r WHERE r.id = ?1";

    public static final String GET_RIDE_BY = "" +
            "SELECT r.*\n" +
            "FROM rides r\n" +
            "INNER JOIN e_vehicles v ON v.id = r.e_vehicle_id\n" +
            "INNER JOIN accounts a ON a.id = r.account_id\n" +
            "WHERE a.id = :account_id \n" +
            "AND v.id = :id AND v.status = 'RENTED';";
}
