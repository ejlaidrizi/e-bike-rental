package seeu.edu.EBikeRental.sql;

public class BankCardSQL {
    public static final String GET_BALANCE = "SELECT balance FROM bank_cards bc WHERE bc.id = ?1";

    public static final String UPDATE_BALANCE = "UPDATE bank_cards SET balance = :balance WHERE id = :id";

    public static final String GET_DEFAULT = "SELECT balance FROM bank_cards bc WHERE account_id=?1 AND default_type='Default'";

    public static final String GET_ID_DEFAULT = "SELECT id FROM bank_cards bc WHERE account_id=?1 AND default_type='Default'";

    public static final String UPDATE_DEFAULT_TYPE = "UPDATE bank_cards SET default_type=?2 WHERE id = ?1";

    public static final String INSERT = "" +
            "INSERT INTO `bank_cards` " +
            "(`id`, `type`, `default_type`, `card_number`, `cardholder_name`, `cvv`, `balance`, `expiration_date`, `account_id`) " +
            "\n" +
            "VALUES (NULL, ?1, NULL, ?2, ?3, ?4, ?5, ?6, ?7 )";
}
