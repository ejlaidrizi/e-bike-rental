package seeu.edu.EBikeRental.pojo.input;

import java.util.Optional;

public class RentRideInput {

    private String qrCode;
    private Integer accountId;
    private Optional<Integer> rideId;
    private Optional<Float> routeDistance;
    private Optional<Float> calories;
    private Optional<String> location;

    public RentRideInput() {
    }

    public RentRideInput(String qrCode, Integer accountId, Optional<Integer> rideId, Optional<Float> routeDistance,
                         Optional<Float> calories, Optional<String> location) {
        this.qrCode = qrCode;
        this.accountId = accountId;
        this.rideId = rideId;
        this.routeDistance = routeDistance;
        this.calories = calories;
        this.location = location;
    }

    public String getQrCode() {
        return qrCode;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }

    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }

    public Optional<Integer> getRideId() {
        return rideId;
    }

    public void setRideId(Optional<Integer> rideId) {
        this.rideId = rideId;
    }

    public Optional<Float> getRouteDistance() {
        return routeDistance;
    }

    public void setRouteDistance(Optional<Float> routeDistance) {
        this.routeDistance = routeDistance;
    }

    public Optional<Float> getCalories() {
        return calories;
    }

    public void setCalories(Optional<Float> calories) {
        this.calories = calories;
    }

    public Optional<String> getLocation() {
        return location;
    }

    public void setLocation(Optional<String> location) {
        this.location = location;
    }
}
