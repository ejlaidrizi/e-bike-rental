package seeu.edu.EBikeRental.pojo.input;

public class AccountInput {

    private String fullName;
    private String email;
    private String phoneNumber;

    public AccountInput() {
    }

    public AccountInput(String fullName, String email, String phoneNumber) {
        this.fullName = fullName;
        this.email = email;
        this.phoneNumber = phoneNumber;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String validate() {
        if (fullName == null || fullName.isEmpty()) {
            return "First and Lastname required!";
        }
        if (email == null || email.isEmpty()) {
            return "Email not provided!";
        }
        if (phoneNumber == null || phoneNumber.isEmpty()) {
            return "Phone number not provided!";
        }
        return null;
    }
}
