package seeu.edu.EBikeRental.pojo.input;

import seeu.edu.EBikeRental.pojo.enums.BankCardType;

public class BankCardInput {

    private BankCardType type;
    private String cardNumber;
    private String cardHolderName;
    private Integer cvv;
    private Float balance;
    private String expirationDate;

    public BankCardInput() {
    }

    public BankCardInput(String cardNumber, String cardHolderName, Integer cvv, Float balance, String expirationDate) {
        this.cardNumber = cardNumber;
        this.cardHolderName = cardHolderName;
        this.cvv = cvv;
        this.balance = balance;
        this.expirationDate = expirationDate;
    }

    public BankCardType getType() {
        return type;
    }

    public void setType(BankCardType type) {
        this.type = type;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getCardHolderName() {
        return cardHolderName;
    }

    public void setCardHolderName(String cardHolderName) {
        this.cardHolderName = cardHolderName;
    }

    public Integer getCvv() {
        return cvv;
    }

    public void setCvv(Integer cvv) {
        this.cvv = cvv;
    }

    public Float getBalance() {
        return balance;
    }

    public void setBalance(Float balance) {
        this.balance = balance;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }
}
