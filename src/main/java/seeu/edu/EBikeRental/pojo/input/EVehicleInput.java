package seeu.edu.EBikeRental.pojo.input;

public class EVehicleInput {

    private String modelName;
    private String brandName;
    private String eType;
    private Float maxSpeed;
    private Integer maxBatteryCapacity;
    private Integer currentBatteryCapacity;
    private Float rangePerCharge;
    private Float minutePrice;
    private String status;
    private String location;
    private String QRCode;

    public EVehicleInput() {
    }

    public EVehicleInput(String modelName, String brandName, String eType, Float maxSpeed, Integer maxBatteryCapacity, Integer currentBatteryCapacity, Float rangePerCharge, Float minutePrice, String status, String location, String QRCode) {
        this.modelName = modelName;
        this.brandName = brandName;
        this.eType = eType;
        this.maxSpeed = maxSpeed;
        this.maxBatteryCapacity = maxBatteryCapacity;
        this.currentBatteryCapacity = currentBatteryCapacity;
        this.rangePerCharge = rangePerCharge;
        this.minutePrice = minutePrice;
        this.status = status;
        this.location = location;
        this.QRCode = QRCode;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String geteType() {
        return eType;
    }

    public void seteType(String eType) {
        this.eType = eType;
    }

    public Float getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(Float maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public Integer getMaxBatteryCapacity() {
        return maxBatteryCapacity;
    }

    public void setMaxBatteryCapacity(Integer maxBatteryCapacity) {
        this.maxBatteryCapacity = maxBatteryCapacity;
    }

    public Integer getCurrentBatteryCapacity() {
        return currentBatteryCapacity;
    }

    public void setCurrentBatteryCapacity(Integer currentBatteryCapacity) {
        this.currentBatteryCapacity = currentBatteryCapacity;
    }

    public Float getRangePerCharge() {
        return rangePerCharge;
    }

    public void setRangePerCharge(Float rangePerCharge) {
        this.rangePerCharge = rangePerCharge;
    }

    public Float getMinutePrice() {
        return minutePrice;
    }

    public void setMinutePrice(Float minutePrice) {
        this.minutePrice = minutePrice;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getQRCode() {
        return QRCode;
    }

    public void setQRCode(String QRCode) {
        this.QRCode = QRCode;
    }

    public String validate() {
        if (modelName == null || modelName.isEmpty()) {
            return "Model Name is required!";
        }
        if (eType == null || eType.isEmpty()) {
            return "Vehicle type is required!";
        }
        if (maxBatteryCapacity == null) {
            return "Battery capacity is required!";
        }
        if (rangePerCharge == null || rangePerCharge.isNaN() || rangePerCharge < 0) {
            return "Invalid range per charge provided!";
        }
        if (minutePrice == null || minutePrice.isNaN() || minutePrice < 0) {
            return "Invalid price per minute provided!";
        }
        return null;
    }
}
