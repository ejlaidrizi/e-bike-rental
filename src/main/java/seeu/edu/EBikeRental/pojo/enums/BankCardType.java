package seeu.edu.EBikeRental.pojo.enums;

public enum BankCardType {
    DEBIT,
    VISA,
    MASTERCARD,
    MAESTRO,
    CREDIT,
    OTHER
}
