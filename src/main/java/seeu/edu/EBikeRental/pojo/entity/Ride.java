package seeu.edu.EBikeRental.pojo.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Table(name = "rides")
public class Ride {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "route_distance")
    @JsonProperty("route_distance")
    private Float routeDistance;

    @Column(name = "start_time")
    @JsonProperty("start_time")
    private LocalDateTime startTime;

    @Column(name = "end_time")
    @JsonProperty("end_time")
    private LocalDateTime endTime;

    @Column(name = "time_rented")
    @JsonProperty("time_rented")
    private String timeRented;

    @Column(name = "calories")
    private Float calories;

    @Column(name = "payment")
    private Float payment;

    @ManyToOne
    @JoinColumn(name = "account_id")
    @JsonManagedReference
    private Account account;

    @ManyToOne
    @JoinColumn(name = "e_vehicle_id")
    @JsonManagedReference
    @JsonProperty("vehicle")
    private EVehicle eVehicle;

    public Ride() {
    }

    public Ride(Integer id, Float routeDistance, LocalDateTime startTime, LocalDateTime endTime, String timeRented,
                Float calories, Float payment, Account account, EVehicle eVehicle) {
        this.id = id;
        this.routeDistance = routeDistance;
        this.startTime = startTime;
        this.endTime = endTime;
        this.timeRented = timeRented;
        this.calories = calories;
        this.payment = payment;
        this.account = account;
        this.eVehicle = eVehicle;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Float getRouteDistance() {
        return routeDistance;
    }

    public void setRouteDistance(Float routeDistance) {
        this.routeDistance = routeDistance;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    public String getTimeRented() {
        return timeRented;
    }

    public void setTimeRented(String timeRented) {
        this.timeRented = timeRented;
    }

    public Float getCalories() {
        return calories;
    }

    public void setCalories(Float calories) {
        this.calories = calories;
    }

    public Float getPayment() {
        return payment;
    }

    public void setPayment(Float payment) {
        this.payment = payment;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public EVehicle getEVehicle() {
        return eVehicle;
    }

    public void setEVehicle(EVehicle eVehicle) {
        this.eVehicle = eVehicle;
    }
}
