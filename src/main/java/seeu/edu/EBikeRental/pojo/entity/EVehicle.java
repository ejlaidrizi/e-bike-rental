package seeu.edu.EBikeRental.pojo.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Set;

@Entity
@Table(name = "e_vehicles")
public class EVehicle {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "model_name")
    @JsonProperty("model_name")
    private String modelName;

    @Column(name = "brand_name")
    @JsonProperty("brand_name")
    private String brandName;

    @Column(name = "e_type")
    @JsonProperty("type")
    private String eType;

    @Column(name = "max_speed")
    @JsonProperty("max_speed")
    private Float maxSpeed;

    @Column(name = "max_battery_capacity")
    @JsonProperty("max_battery_capacity")
    private Integer maxBatteryCapacity;

    @Column(name = "current_battery_capacity")
    @JsonProperty("current_battery_capacity")
    private Integer currentBatteryCapacity;

    @Column(name = "range_per_charge")
    @JsonProperty("range_per_charge")
    private Float rangePerCharge;

    @Column(name = "minute_price")
    @JsonProperty("minute_price")
    private Float minutePrice;

    @Column(name = "status")
    private String status;

    @Column(name = "location")
    private String location;

    @Column(name = "QR_code")
    @JsonProperty("QR_code")
    private String QRCode;

    @OneToMany(mappedBy = "eVehicle")
    @JsonBackReference
    private Set<Ride> rides;

    public EVehicle() {
    }

    public EVehicle(Integer id, String modelName, String brandName, String eType, Float maxSpeed, Integer maxBatteryCapacity,
                    Integer currentBatteryCapacity, Float rangePerCharge, Float minutePrice, String status, String location,
                    String QRCode, Set<Ride> rides) {
        this.id = id;
        this.modelName = modelName;
        this.brandName = brandName;
        this.eType = eType;
        this.maxSpeed = maxSpeed;
        this.maxBatteryCapacity = maxBatteryCapacity;
        this.currentBatteryCapacity = currentBatteryCapacity;
        this.rangePerCharge = rangePerCharge;
        this.minutePrice = minutePrice;
        this.status = status;
        this.location = location;
        this.QRCode = QRCode;
        this.rides = rides;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String geteType() {
        return eType;
    }

    public void seteType(String eType) {
        this.eType = eType;
    }

    public Float getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(Float maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public Integer getMaxBatteryCapacity() {
        return maxBatteryCapacity;
    }

    public void setMaxBatteryCapacity(Integer maxBatteryCapacity) {
        this.maxBatteryCapacity = maxBatteryCapacity;
    }

    public Integer getCurrentBatteryCapacity() {
        return currentBatteryCapacity;
    }

    public void setCurrentBatteryCapacity(Integer currentBatteryCapacity) {
        this.currentBatteryCapacity = currentBatteryCapacity;
    }

    public Float getRangePerCharge() {
        return rangePerCharge;
    }

    public void setRangePerCharge(Float rangePerCharge) {
        this.rangePerCharge = rangePerCharge;
    }

    public Float getMinutePrice() {
        return minutePrice;
    }

    public void setMinutePrice(Float minutePrice) {
        this.minutePrice = minutePrice;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getQRCode() {
        return QRCode;
    }

    public void setQRCode(String QRCode) {
        this.QRCode = QRCode;
    }

    public Set<Ride> getRides() {
        return rides;
    }

    public void setRides(Set<Ride> rides) {
        this.rides = rides;
    }
}
